# -*- coding:utf-8 -*-

import hashlib
import datetime
import uuid
import traceback

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, JSON
from sqlalchemy.schema import ForeignKey

from db_utils import create_dbengine

Base = declarative_base()


class SourceInfo(Base):
    __tablename__ = 'sourceinfo'

    id = Column(Integer, primary_key=True)
    caption = Column(String)
    subcaption = Column(String)
    page_count = Column(Integer)


def rewrite_tables():
    e = create_dbengine()
    try:
        SourceInfo.__table__.drop(e)
    except:

        # print(traceback.format_exc())

    Base.metadata.drop_all(e)
    Base.metadata.create_all(e)
