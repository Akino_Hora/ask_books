# -*- coding:utf-8 -*-

import hashlib
import datetime
import uuid

from sqlalchemy.sql.expression import or_, and_

from models import *

def calc_hash(item):
    try:
        s = item.__tablename__ + str(item.id)
        return hashlib.sha256(s.encode('utf-8')).hexdigest()
    except:
        print('Error: input item is not table object')
        return

def get_session(sessionKey, dbsession):
    return dbsession.query(Session).filter(Session.key == sessionKey).first()


def create_session(user_id, device_id, dbsession):
    session = dbsession.query(Session).filter_by(
                user_id=user_id,
                device_id=device_id,
                deleted=None
            ).first()
    if session:
        session.deleted = datetime.datetime.utcnow()
        dbsession.add(session)
    session_new = Session(
        user_id=user_id,
        device_id=device_id,
        key = str(uuid.uuid4()))
    dbsession.add(session_new)
    dbsession.commit()
    return dbsession.query(Session).filter_by(
                user_id=user_id,
                device_id=device_id,
                deleted=None,
            ).first()


def get_talk(user_id, dbsession):
    talk_old = dbsession.query(Talk).filter(
        or_(
            Talk.user_id_1 == user_id, 
            Talk.user_id_2 == user_id
        )
        ).filter(Talk.disconnected == None).first()

    if talk_old:
        talk_old.disconnected = datetime.datetime.utcnow()
        dbsession.add(talk_old)

    talk_new = Talk(user_id_1=user_id)

    dbsession.add(talk_new)
    dbsession.commit()

    return dbsession.query(Talk).filter(
        or_(
            Talk.user_id_1 == user_id, 
            Talk.user_id_2 == user_id
        )
        ).filter(Talk.disconnected == None).first()


def get_device_with_id(device_id, dbsession):
    return dbsession.query(Device).filter(Device.device_uid == device_id).first()

def get_user_with_device_id(device_id, dbsession):
    return dbsession.query(User).filter(User.device_id == device_id).first()

def create_device(device_uid, platform, platform_version, dbsession):
    device = Device(
        device_uid=device_uid,
        platform=platform,
        data={'platform_version': platform_version,}
    )
    dbsession.add(device)
    dbsession.commit()

    return dbsession.query(Device).filter(Device.device_uid == device_uid).first()


def create_user(device_id, dbsession):
    '''
    создаем User на сервере
    создаем User в БД
    вытаскиваем User из БД (с присвоенным id)
    считаем хеш, учитывая этот id
    возвращаем User в БД
    '''

    user = User(device_id=device_id)
    dbsession.add(user)
    dbsession.commit()

    user = dbsession.query(User).filter(User.device_id == device_id).first()
    user.connect_hash = calc_hash(user)
    dbsession.add(user)
    dbsession.commit()

    return user



