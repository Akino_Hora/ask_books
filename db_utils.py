# -*- coding:utf-8 -*-

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from settings import DB_PATH

def create_dbengine():
    return create_engine(DB_PATH)

def create_dbsession():
    e = create_dbengine()
    Session = sessionmaker(bind=e)
    return Session()