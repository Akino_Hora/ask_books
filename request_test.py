
import requests
import json

ConnectionInfo = {
    'id': 'string',
    'room_id': 'string',
    'configuration': {
        'iceServers',
    }
}

data = {
    'device_id': '100501',  # поле device_uid в Device
    'platform_version': 'good version',
    'sessionKey': '16ccac7b-57fb-4454-bdb0-5c76d359ce8a',
    'platform': 'ios',
}

# r = requests.post('http://127.0.0.1:8888/app/login', data=data)
r = requests.post('http://127.0.0.1:8888/chat/request', data=data)
# r = requests.post('http://127.0.0.1:8888/chat/connected', data=data)

try:
    print(json.loads(r.text))
except:
    print((r.text))