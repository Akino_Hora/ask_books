# -*- coding:utf-8 -*-
import json

import tornado.ioloop
import tornado.web
import tornado.escape
from sqlalchemy import *

from models import *
from db_utils import *
from utils import *

dbsession = create_dbsession()

class AppLoginHandler(tornado.web.RequestHandler):
    def post(self):

        self.write()

handlers = [
    (r"/app/login", AppLoginHandler),
]

def make_app():
    return tornado.web.Application(handlers)

def create_data():
    sourceinfo = SourceInfo()

    dbsession.add(sourceinfo)
    dbsession.commit()
    dbsession.close()

def create_new_table(metadata):
    metadata = MetaData()

    with create_dbengine().connect() as conn:
        conn.execute(
            data_table.insert(),
            data = {"key1": "value1", "key2": "value2"}
        )

if __name__ == '__main__':
    rewrite_tables()
    create_data()
    
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()

